import * as Type from './product-types';

export const productState = {
    currentProduct: {},
    products: [],

    currentPage: 1,
    prodsPerPage: 4,
    totalPage: 1,

    isCreateOpen: false,
    isEditOpen: false,
    isDeleteOpen: false,

    loading: false,
    error: null
};

export const productReducer = (state = productState, action) => {
    switch (action.type) {
        case Type.PRODUCT_REQUEST:
            return { ...state, loading: true };
        case Type.GET_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: action.products,
                currentPage: action.currentPage,
                prodsPerPage: action.prodsPerPage,
                totalPage: action.totalPage,
                loading: false
            };
        case Type.GET_SINGLE_PRODUCT_SUCCESS:
            return { ...state, currentProduct: action.payload, loading: false }

        case Type.SET_CREATE_MODAL_STATE:
            return { ...state, isCreateOpen: !state.isCreateOpen }
        case Type.SET_EDIT_MODAL_STATE:
            return { ...state, isEditOpen: !state.isEditOpen }
        case Type.SET_DELETE_MODEL_STATE:
            return { ...state, isDeleteOpen: !state.isDeleteOpen }
        case Type.SET_CURRENT_PRODUCT:
            return { ...state, currentProduct: action.payload }
        case Type.SET_CURRENT_PAGE:
            return { ...state, currentPage: action.payload }

        case Type.CREATE_PRODUCT_SUCCESS:
            return { ...state }
        case Type.UPDATE_PRODUCT_SUCCESS:
            return { ...state }
        case Type.DELETE_PRODUCT_SUCCESS:
            return { ...state }

        case Type.PRODUCT_FAILURE:
            return { ...state, error: action.error, loading: false };
        default:
            return state;
    }
};
