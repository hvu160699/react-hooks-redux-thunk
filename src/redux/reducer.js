import { combineReducers } from "redux";
import { productReducer } from "../store/product/product-reducer";

const rootReducer = combineReducers({
  productReducer
});

export default rootReducer;
