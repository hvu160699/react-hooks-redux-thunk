export const BASE_URL = 'http://localhost:3030/'

export const PRODUCT = 'product/'

export const PRODUCT_BY_ID = ObjectId => `product/${ObjectId}`