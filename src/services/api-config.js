import { BASE_URL } from "./api-link";

export function callApi(
    endpoint,
    method = "GET",
    body,
    header = {
        Accept: "application/json",
        "Content-Type": "application/json",
        // "Access-Control-Allow-Origin": "*",
        // "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
        // "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    }
) {
    return new Promise((resolve, reject) => {
        fetch(`${BASE_URL}${endpoint}`, {
            headers: header,
            method,
            body: JSON.stringify(body)
        })
            .then(async response => {
                const result = await response.json();
                resolve(result)
            })
            .catch(error => {
                reject(error);
            });
    });
}
