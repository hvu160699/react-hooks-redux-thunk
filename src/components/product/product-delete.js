import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Dialog, DialogTitle, DialogContentText, DialogContent, DialogActions, Button } from '@material-ui/core';
import { deleteProduct, loadAllProducts, setDeleteModalState } from '../../store/product/product-action';

export const ProductDelete = () => {
    const state = useSelector(state => state.productReducer)
    const dispatch = useDispatch();

    const handleConfirm = _id => {
        dispatch(deleteProduct(_id))
        dispatch(setDeleteModalState())

        setTimeout(() => dispatch(loadAllProducts()), 1000)
    }

    return (
        state.currentProduct && <Dialog
            open={state.isDeleteOpen}
            onClose={() => dispatch(setDeleteModalState())}
            aria-labelledby="alert-dialog-title"
            className="delete-dialog"
        >
            <DialogTitle className="delete-dialog__title">{state.currentProduct._id}</DialogTitle>
            <DialogContent className="delete-dialog__content">
                <DialogContentText id="alert-dialog-description">
                    Are you sure to delete this product?
                </DialogContentText>
            </DialogContent>
            <DialogActions className="delete-dialog__actions">
                <Button color="primary" onClick={() => dispatch(setDeleteModalState())}>
                    Disagree
                </Button>
                <Button onClick={() => handleConfirm(state.currentProduct._id)} color="primary" autoFocus>
                    Agree
            </Button>
            </DialogActions>
        </Dialog>
    )
}