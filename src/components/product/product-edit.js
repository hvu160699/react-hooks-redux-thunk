import React, { useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button } from '@material-ui/core';
import { updateProduct, loadAllProducts, setEditModalState } from '../../store/product/product-action';

export const ProductEdit = () => {
    const state = useSelector(state => state.productReducer)
    const dispatch = useDispatch();

    const productNewNameRef = useRef()
    const productNewDescriptionRef = useRef()

    const handleSubmit = e => {
        e.preventDefault()
        const updateProductObj = {
            _id: state.currentProduct._id,
            name: productNewNameRef.current.value,
            description: productNewDescriptionRef.current.value
        }
        dispatch(updateProduct(updateProductObj))
        dispatch(setEditModalState())

        setTimeout(() => dispatch(loadAllProducts()), 1000)
    }

    return (
        state.currentProduct &&
        <Dialog
            key={state.currentProduct._id}
            open={state.isEditOpen}
            onClose={() => dispatch(setEditModalState())}
            aria-labelledby="form-dialog-title"
            className="update-dialog">
            <DialogTitle className="update-dialog__title">{state.currentProduct._id}</DialogTitle>
            <form autoComplete="off">
                <DialogContent className="update-dialog__content">
                    <TextField
                        inputRef={productNewNameRef}
                        defaultValue={state.currentProduct.name}
                        variant="outlined"
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Name"
                        type="text"
                        fullWidth
                    />
                    <TextField
                        inputRef={productNewDescriptionRef}
                        defaultValue={state.currentProduct.description}
                        variant="outlined"
                        margin="dense"
                        id="description"
                        label="Description"
                        type="text"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions className="update-dialog__actions">
                    <Button onClick={() => dispatch(setEditModalState())} color="primary">
                        Cancel
                </Button>
                    <Button color="primary" onClick={e => handleSubmit(e)}>
                        Update
                </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
}